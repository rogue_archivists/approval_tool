## An Licence Approval System

## Overview
A program to aid in the approval of the licenses for new third-party software by a legal department.

## Outline
1. Request package to be approved. You select package type, type in the name, and the version and it looks it up on Nuget, NPM, etc. Might start with just a single package type at first since most approvals are Nuget.
2. It looks to see if the package has been approved before. Do the listing of licenses and such.
3. Allows someone to approve it.

Doesn't do anymore integration than that. Just acts as an approval database.
Any hooking into things etc. would be additions.

## Feature
- [ ] show text diff of license
    - [ ] between requested  and prior, if prior was approved
    - [ ] between requested  and standard license dictionary
- [ ] Dependency
    - [ ] included in approval diff
    - [ ] find dependency for
        - [ ] NuGet
        - [ ] NPM
- [ ] auto-approval base on configuration
    - [ ] when no change to prior
    - [ ] when no change from standard license
- [ ] Stretch Goals
    - [ ] managing open versions of lib /  deprecation
    - [ ] lib vs product tracking
    - [ ] pulling bug/vulnerability notices (NPM provides)
 
## Tools
* NodeJS
* Git (approval repo)
 
## Style
* functional suitable for lambda deployments on cloud platform
* storage
* immutalbe
* document
 
## Configs
* YAML
 
## Diagrams

```plantuml
@startuml  usecase
 
left to right direction
skinparam packageStyle rect
 
actor "Developer" as dev
actor "Legal" as legal
 
rectangle {
    usecase "Manage Approval" as manage
    usecase "Request Approval" as request
    usecase "Compaire Licence" as lice
 
    (request) <. (manage) : include
    (manage) <. (lice)  : extents
}
 
dev -- request
manage -- legal
lice -- legal
 
@enduml
```
 
```plantuml
boundary "Approval Service" as apprv_mngr
 
box
    collections master
    collections request
end box
 
boundary check
 
title new software request
-> apprv_mngr : software reuqest
apprv_mngr -> master : branch
master -> request : open branch
activate request
 
    apprv_mngr -> request: commit "software-lable.yaml"
    apprv_mngr -> request: commit deps
   
    request -> check : test check "master...request"
 
    check -> apprv_mngr : dependency complete
 
    apprv_mngr -> request : calc diff
    <- apprv_mngr : request approval
deactivate request
```

```plantuml
title Remove Software

boundary "Approval Service" as apprv_mngr
 
box
    collections master
    collections request
end box
 
boundary check
 
-> apprv_mngr : remove reuqest
apprv_mngr -> master : branch
master -> request : open branch
activate request
 
    apprv_mngr ->x request: git rm "software-label.yaml"
 
    apprv_mngr -> check : check "request" "**/*"
    check -> apprv_mngr : dependency resutls
 
    apprv_mngr ->
 
deactivate request
 
```
 
## Examples Files
```yaml
# file: _some_software_-_SHA1_.yaml
name: "Some Third-party software"
 
# full hash of commit
SHA1: 0xFEDCBA9876543210
 
# lables should not include relative commit-ish names
labels: ["v1.2.3", "some_new_feature"]
 
licence: |4
    MIT License
    
    Copyright (c) <year> <author>
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.



dependencies:
    - {name: "dep1", hash: 0xF987765 }
    - {name: "dep2", hash: 0xF9A7765 }
    - {name: "dep3", hash: 0xF9B7765 }

```